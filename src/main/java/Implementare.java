import javax.swing.text.html.HTMLDocument;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Implementare {
    public void cerinte(){
        ArrayList<MonitoredData> monitoredData=readFile();
        monitorizedDate(monitoredData);
        activityCount(monitoredData);
        durata(monitoredData);
        noOfActivityPerDay(monitoredData);
        durataTotalaActivitate(monitoredData);


    }

    public Map<String, Integer> activityCount(ArrayList<MonitoredData> monitoredData){
        System.out.println("Numarul activitatiilor din fiser:");
        Map<String, Integer> map=monitoredData.stream().collect(Collectors.toMap(MonitoredData::getActivity,m->1,Integer::sum));

        Iterator<String> iterator=map.keySet().iterator();
        Iterator<Integer> iterator1=map.values().iterator();
        while (iterator.hasNext() && iterator1.hasNext()){
            System.out.println(iterator.next()+"\t\t"+iterator1.next());
        }

        return map;
    }


    public void monitorizedDate(ArrayList<MonitoredData> monitoredData){

        int count=(int) monitoredData.stream().map(m->m.getStartTime().getDayOfYear()).distinct().count();
        System.out.println("Numarul total de zile monitorizate: "+count);
    }

    public void durata(ArrayList<MonitoredData> monitoredData){
        for (int i=0;i<monitoredData.size();i++){
            System.out.println("Activitetea: "+monitoredData.get(i).getActivity()+Duration.between(monitoredData.get(i).getEndTime(),monitoredData.get(i).getStartTime()));
        }
    }

    public void durataTotalaActivitate(ArrayList<MonitoredData> monitoredData){
        System.out.println("Durata totala pentru fiecare activitatea: ");
        Map<Object,List<Object>> map = monitoredData.stream().collect((Collectors.groupingBy(m->m.getActivity(),Collectors.mapping(t->t.getSec(),Collectors.toList()))));

        Iterator<Object> iterator=map.keySet().iterator();
        Iterator<List<Object>> listIterator=map.values().iterator();

        while (iterator.hasNext() && listIterator.hasNext()){
            System.out.println(iterator.next()+"\t\t"+listIterator.next());
        }
    }

    public void noOfActivityPerDay(ArrayList<MonitoredData> monitoredData){
        System.out.println("Numarul de activitati din fiecare zi: ");
        Map<Object,Map<Object,Long>> map = monitoredData.stream().collect(Collectors.groupingBy(a->a.getStartTime().getDayOfYear(),Collectors.groupingBy(m->m.getActivity(),Collectors.counting())));

        Iterator<Object> iterator=map.keySet().iterator();
        Iterator<Map<Object,Long>> iterator1= map.values().iterator();

        while (iterator.hasNext() && iterator1.hasNext()){
            System.out.println(iterator.next()+"\t\t"+iterator1.next());
        }
    }








    public ArrayList<MonitoredData> readFile()
    {
        String fileName = "C:\\Users\\Deni\\Desktop\\PT\\PT2019\\pt2019_30222_idu_irina-denisa_assignment_5\\activitati.txt";
        ArrayList<MonitoredData> list_data=new ArrayList<>();
        List<String> list_file=new ArrayList<String>();

        try (Stream<String> stream = Files.lines(Paths.get(fileName)))
        {
            list_file=stream.collect(Collectors.toList());
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        list_data=(ArrayList<MonitoredData>) list_file.stream().map(s->s.split("\t\t")).map(array->new MonitoredData(LocalDateTime.parse(array[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),LocalDateTime.parse(array[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),array[2])).collect(Collectors.toList());
        return list_data;
    }
}
