import java.time.Duration;
import java.time.LocalDateTime;

public class MonitoredData {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activity;


    public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activity){
        this.startTime=startTime;
        this.endTime=endTime;
        this.activity=activity;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public String getActivity() {
        return activity;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public Long getSec(){
        Duration duration = Duration.between(getStartTime(), getEndTime());
        return duration.getSeconds();

    }



}
